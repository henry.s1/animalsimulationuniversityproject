module so.henry.animalSimulation {
    requires javafx.controls;
    requires javafx.fxml;

    opens so.henry.simulation to javafx.fxml;
    exports  so.henry.simulation;

}