package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the AnFood class
 * it is the parent of all food entities
 * @author Henry So
 * @version 1.0 Final
 */
public abstract class AnFood extends AnEntity 
{

	/**
	 * Constructor for AnFood
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AnFood(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) 
	{
		super(s, sy, h, v, u, e, w,i);
		// TODO Auto-generated constructor stub
	}
	
		
	
}

