package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the movingfood class
 * it is a child of food and
 * this type of food and move
 * @author Henry So
 * @version 1.0 Final
 */
public class AnMovingFood extends AnFood {
	/**
	 * Variables for AnMovingFood to use
	 */
	private int foodIndex;
	private boolean rest = false;
	private boolean poison = false;
	private int poisonCount = 0;


	/**
	 * Constructor for AnMovingFood
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AnMovingFood(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) {
		super(s, sy, h, v, u, e, w, i);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Smell food function for movingfood to search for food in a given range
	 * @param d			Direction in which movingfood is facing
	 * @param range		Range that the movingfood can smell from
	 * @return boolean	True if ther eis food in that direction else return false
	 */
	boolean smellFood(ADirection d, int range)
	{
		AWorld tempWorld = getWorld();
		int tempX = getxPos(), tempY = getyPos();
		boolean isFood = true;
		boolean isObs = true;
		for(int offset = 0; offset <= range; offset = offset + 10)
		{	
			if(d.equals(ADirection.N))
			{	
				isFood = tempWorld.isHerbFood(tempWorld, tempX, tempY - offset);
				isObs = tempWorld.isObs(tempWorld, tempX, tempY - offset);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX, tempY - offset);
					return true;
				}
			}
			else if(d.equals(ADirection.S))
			{
				isFood = tempWorld.isHerbFood(tempWorld, tempX, tempY + offset);
				isObs = tempWorld.isObs(tempWorld, tempX, tempY + offset);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX, tempY + offset);
					return true;
				}											
			}
			else if(d.equals(ADirection.E))
			{
				isFood = tempWorld.isHerbFood(tempWorld, tempX + offset, tempY);
				isObs = tempWorld.isObs(tempWorld, tempX + offset, tempY);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX + offset, tempY);
					return true;
				}	
			}
			else if(d.equals(ADirection.W))
			{
				isFood = tempWorld.isHerbFood(tempWorld, tempX - offset, tempY);
				isObs = tempWorld.isObs(tempWorld, tempX - offset, tempY);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX - offset, tempY);
					return true;
				}
			}						
		}
		return false;
	}
	
	/**
	 * Get the direction a food is in
	 * @param range			The range that the movingfood can sense from
	 * @return	ADirection	The direction that the food is in
	 */
	ADirection getDirectionFood(int range)
	{
		
		if(smellFood(ADirection.N, range))
		{
			return ADirection.N;
			
		}
		else if(smellFood(ADirection.S,range))
		{
			return ADirection.S;
		}
		else if(smellFood(ADirection.E,range))
		{
			return ADirection.E;
		}else if(smellFood(ADirection.W,range))
		{
			return ADirection.W;
		}
		else
		{
			return ADirection.None;
		}
		
	}
		
	/**
	 * Move movingfood in a given direction
	 * @param d				The direction to move in
	 * @param tempWorld		The world to move in
	 */
	void moveDirection(ADirection d, AWorld tempWorld)
	{
		int tempX = getxPos(), tempY = getyPos();
		if(d.equals(ADirection.N))
		{
			if(tempWorld.canMove(tempWorld, tempX, tempY - 10))
				{			
					if((tempY - 10) > 0)
					{
						setyPos(tempY-10);
					}	
				}
		}
		else if(d.equals(ADirection.S))
		{
			if(tempWorld.canMove(tempWorld, tempX, tempY + 10))
			{			
				if((tempY + 10) < tempWorld.getVertiSize())
				{
					setyPos(tempY + 10);
				}
			}		
		}
		else if(d.equals(ADirection.E))
		{
			if(tempWorld.canMove(tempWorld, tempX + 10 , tempY))
			{			
				if((tempX + 10) < tempWorld.getHoriSize())
				{
					setxPos(tempX + 10);
				}
			}	
		}
		else if(d.equals(ADirection.W))
		{
			if(tempWorld.canMove(tempWorld, tempX - 10 , tempY))
			{					
				if((tempX - 10) > 0)
				{
					setxPos(tempX - 10);
				}
			}	
			
		}
	}
	
	/**
	 * findHole function for movingfood to search for hole in a given range
	 * @param d			Direction in which movingfood is facing
	 * @param range		Range that the movingfood can sense from
	 * @return boolean	True if there is hole in that direction else return false
	 */
	boolean findHole(ADirection d, int range)
	{
		AWorld tempWorld = getWorld();
		int tempX = getxPos(), tempY = getyPos();
		boolean isObs = true;
		for(int offset = 0; offset <= range; offset = offset + 10)
		{	
			if(d.equals(ADirection.N))
			{	
				
				isObs = tempWorld.isObs(tempWorld, tempX, tempY - offset);
				if(isObs)
				{
					return true;
				}
			}
			else if(d.equals(ADirection.S))
			{
				isObs = tempWorld.isObs(tempWorld, tempX, tempY + offset);
				if(isObs)
				{
					return true;
				}											
			}
			else if(d.equals(ADirection.E))
			{
				
				isObs = tempWorld.isObs(tempWorld, tempX + offset, tempY);
				if(isObs)
				{
					return true;
				}	
			}
			else if(d.equals(ADirection.W))
			{
				isObs = tempWorld.isObs(tempWorld, tempX - offset, tempY);
				if(isObs)
				{
					return true;
				}
			}						
		}
		return false;
	}
	
	/**
	 * Get the direction a hole is in
	 * @param range			The range that the movingfood can sense from
	 * @return	ADirection	The direction that the hole is in
	 */
	ADirection getHoleDirection(int range)
	{
		
		if(findHole(ADirection.N, range))
		{
			return ADirection.N;
			
		}
		else if(findHole(ADirection.S,range))
		{
			return ADirection.S;
		}
		else if(findHole(ADirection.E,range))
		{
			return ADirection.E;
		}else if(findHole(ADirection.W,range))
		{
			return ADirection.W;
		}
		else
		{
			return ADirection.None;
		}
		
	}
		
	/**
	 * Move movingfood in a given direction towards resthole
	 * @param d				The direction to move in
	 * @param tempWorld		The world to move in
	 */
	void restHole(ADirection d, AWorld tempWorld)
	{
		int tempX = getxPos(), tempY = getyPos(), tempEnergy = getEnergy();
		if(d.equals(ADirection.N))
		{
			if(tempWorld.isHole(tempWorld, tempX, tempY - 10))
			{
				if(tempWorld.entities.get(tempWorld.getEntityAt(tempX, tempY - 10)) instanceof AnRabbitHole)
				{
					if((tempY - 20) > 0)
					{
						setyPos(tempY - 2);
						setEnergy(tempEnergy + 50);
					}
				}
				else if(tempWorld.entities.get(tempWorld.getEntityAt(tempX, tempY - 10)) instanceof AnHole)
				{
					tempWorld.entities.remove(tempWorld.getEntityAt(tempX, tempY - 10));
					tempWorld.addRabbitHoleAt(tempX, tempY - 10);
				}
			}
			else
			{
				moveDirection(d, tempWorld);
			}
				
					
		}
		else if(d.equals(ADirection.S))
		{
			if(tempWorld.isHole(tempWorld, tempX, tempY + 10))
			{			
				if(tempWorld.entities.get(tempWorld.getEntityAt(tempX, tempY + 10)) instanceof AnRabbitHole)
				{
					if((tempY + 20) > 0)
					{
						setyPos(tempY + 20);
						setEnergy(tempEnergy + 50);
					}
				}
				else if(tempWorld.entities.get(tempWorld.getEntityAt(tempX, tempY + 10)) instanceof AnHole)				
				{
					tempWorld.entities.remove(tempWorld.getEntityAt(tempX, tempY + 10));
					tempWorld.addRabbitHoleAt(tempX, tempY + 10);
				}
			}	
			else
			{
				moveDirection(d, tempWorld);
			}
		}
		else if(d.equals(ADirection.E))
		{
			if(tempWorld.isHole(tempWorld, tempX + 10, tempY))
			{		
				if(tempWorld.entities.get(tempWorld.getEntityAt(tempX + 10, tempY)) instanceof AnRabbitHole)
				{
					if((tempX + 20) > 0)
					{
						setxPos(tempX + 20);
						setEnergy(tempEnergy + 50);
					}
				}
				else if(tempWorld.entities.get(tempWorld.getEntityAt(tempX + 10, tempY)) instanceof AnHole)	
				{
					tempWorld.entities.remove(tempWorld.getEntityAt(tempX + 10, tempY));
					tempWorld.addRabbitHoleAt(tempX + 10, tempY);
				}
			}
			else
			{
				moveDirection(d, tempWorld);
			}
		}
		else if(d.equals(ADirection.W))
		{
			if(tempWorld.isHole(tempWorld, tempX - 10, tempY))
			{		
				if(tempWorld.entities.get(tempWorld.getEntityAt(tempX - 10, tempY)) instanceof AnRabbitHole)
				{
					if((tempX - 20) > 0)
					{
						setxPos(tempX - 20);
						setEnergy(tempEnergy + 50);
					}
				}
				else if(tempWorld.entities.get(tempWorld.getEntityAt(tempX - 10, tempY)) instanceof AnHole)	
				{
					tempWorld.entities.remove(tempWorld.getEntityAt(tempX - 10, tempY));
					tempWorld.addRabbitHoleAt(tempX - 10, tempY);
				}
			}
			else
			{
				moveDirection(d, tempWorld);
			}
		}
	}


	/**
	 * @return the foodIndex
	 */
	public int getFoodIndex() {
		return foodIndex;
	}


	/**
	 * @return the rest
	 */
	public boolean isRest() {
		return rest;
	}


	/**
	 * @return the poison
	 */
	public boolean isPoison() {
		return poison;
	}


	
	
	/**
	 * @return the poisonCount
	 */
	public int getPoisonCount() {
		return poisonCount;
	}
	
	/**
	 * @return the rest
	 */
	public boolean getRest() {
		return rest;
	}


	/**
	 * @param foodIndex the foodIndex to set
	 */
	public void setFoodIndex(int foodIndex) {
		this.foodIndex = foodIndex;
	}


	/**
	 * @param rest the rest to set
	 */
	public void setRest(boolean rest) {
		this.rest = rest;
	}


	/**
	 * @param poison the poison to set
	 */
	public void setPoison(boolean poison) {
		this.poison = poison;
	}


	/**
	 * @param poisonCount the poisonCount to set
	 */
	public void setPoisonCount(int poisonCount) {
		this.poisonCount = poisonCount;
	}

	
	
	
}
