package so.henry.simulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * This is the GUIInterface class
 * GUIInterface holds all of the UI
 * and also holds one world in
 * @author Henry So
 * @version 1.0 Final
 */
public class MainApp extends Application
{
    /**
     * Variables for use in the GUI interface
     */
    private int reset = 0, restart = 0, displayMap = 0, pause = 0, running = 0,  displayLifeFood = 0, displayConfig = 0, displayMapInfo =0, foodCount = 0, obsCount = 0, wolfCount = 0;
    private Stage all;
    private Scene scene1, scene2, scene3, scene4, scene5;
    private Canvas canvas = new Canvas();
    private GraphicsContext gc;
    private AWorld theWorld;
    private String line, configuration;
    private File defaultFile = new File("G:\\PC\\simulationFiles\\Configuration.txt");
    private TextField userConfig = new TextField(configuration);
    private final Timeline timeline = new Timeline();
    private KeyFrame simulation;

    /**
     * Creates a folder in a location for configuration to be saved at, Only creates the file if the file is not there
     */
    private void makeDefaultLocation()
    {
        try
        {
            File dir = new File("G:/pc/simulationFiles");
            boolean success = dir.exists();



            if(success)
            {
                System.out.println("Folder created at: " + dir);
            }
            else
            {
                System.err.println("Failed to created at: " + dir + " You dont have permission");
            }
        }
        catch(Exception e)
        {
            System.err.println("Error: " + e.getMessage());
        }
    }

    /**
     * Main method where manifest will search for
     * @param args	the parameter to launch the application
     */
    public static void main(String[] args)
    {
        launch(args);
    }

    /**
     * Checks if the configuration entered is valid
     * @param configuration		The configuration
     * @return	boolean
     */
    @SuppressWarnings("unused")
    public boolean isValidConfiguration(String configuration)
    {
        boolean answer = false;
        if(configuration != null)
        {
            try
            {
                String[] TempArr = configuration.split(" ");
                // removes space from array
                for(int counter = 0; counter < TempArr.length; counter++)
                {
                    if(TempArr[counter].charAt(0) == ' ') // if character at position 0 is space then
                    {
                        TempArr[counter] = TempArr[counter].substring(1, TempArr[counter].length());

                    }
                }
                int hori = Integer.parseInt(TempArr[0]);
                int vert = Integer.parseInt(TempArr[1]);
                int food = Integer.parseInt(TempArr[2]);
                int obst = Integer.parseInt(TempArr[3]);

                for(int counter = 4; counter < TempArr.length; counter = counter + 2)
                {
                    String tempSpecies = TempArr[counter];					// gets species name
                    try
                    {
                        int amount = Integer.parseInt(TempArr[counter+1]);
                        char type = tempSpecies.charAt(0);
                        if(type == 'W' || type == 'M' ||type == 'H' ||type == 'A' ||type == 'N' ||type == 'O' ||type == 'R' ||  type == 'P')
                        {
                            answer = true;
                        }
                        else
                        {
                            answer = false;
                        }
                    }
                    catch(NumberFormatException e)
                    {
                        answer = false;
                        return answer;
                    }

                }
                return answer;

            }
            catch(NumberFormatException e)
            {
                answer = false;
                return answer;

            }
        }
        return answer = true;

    }

    /**
     * Changes the world depending on the configuration
     * @param world		The world to change
     */
    public void fromText(AWorld world)
    {
        gc = canvas.getGraphicsContext2D();
        gc.clearRect(0,0,10000, 10000);
        if(isValidConfiguration(configuration) == false)
        {

            configuration = "600 600 50 100 Wolf 2 MoveFood 20 Hole 20 AnRabbitHole 10 NFood 10 Obs 10 Regrow 20 Poison 30";
            fromText(theWorld);
        }
        else
        {
            String[] TempArr = configuration.split(" ");
            // removes space from array
            for(int counter = 0; counter < TempArr.length; counter++)
            {
                if(TempArr[counter].charAt(0) == ' ') // if character at position 0 is space then
                {
                    TempArr[counter] = TempArr[counter].substring(1, TempArr[counter].length());
                }


            }
            /*put first four element to their respectful variables */

            int hori = Integer.parseInt(TempArr[0]);
            int vert = Integer.parseInt(TempArr[1]);
            int food = Integer.parseInt(TempArr[2]);
            int obst = Integer.parseInt(TempArr[3]);

            theWorld = AWorld.setWorld(hori, vert, 100, food, obst);

            for(int i = 0; i < food; i++)
            {

                theWorld.addNewEntity(theWorld, "NFood", 'N', 'N');

            }

            for(int j = 0; j < obst; j++)
            {
                theWorld.addNewEntity(theWorld, "O", 'O', 'O');

            }

            for(int counter = 4; counter < TempArr.length; counter = counter + 2)
            {
                String tempSpecies = TempArr[counter];					// gets species name
                int numEnt = Integer.parseInt(TempArr[counter + 1]);	// find out how many of the species
                for(int i = numEnt; i > 0; i--)
                {
                    theWorld.addNewEntity(theWorld, tempSpecies, tempSpecies.charAt(0),tempSpecies.charAt(0));									// add entity to world
                }
            }
        }

    }

    /**
     * Finds the default file, if file not there then create it
     * @param file		The file that is targeted
     */
    private void defaultFileTest(File file)
    {
        try
        {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null)
            {
                configuration = line;
            }
            if(configuration == null)
            {
                System.err.println("Unable to open file at: " + file + "\nSetting Default configuration to 100 height, 100 width, %10 food, 20% lifeforms" );
                try
                {
                    File tempFile = new File("G:\\pc\\simulationFiles\\Configuration.txt");
                    tempFile.createNewFile();
                    try
                    {
                        FileWriter fileWriter = null;
                        fileWriter = new FileWriter(tempFile);
                        fileWriter.write("600 600 50 100 Wolf 2 MoveFood 20 Hole 20 AnRabbitHole 10 NFood 10 Obs 10 Regrow 20 Poison 30");
                        fileWriter.close();
                    }
                    catch (IOException ex)
                    {
                        System.err.println("Cannot Create file, Error: "+ ex);
                    }

                }
                catch (IOException d)
                {
                    System.err.println("Cannot create file, Error" + d);
                }
            }
            bufferedReader.close();


        }
        catch (FileNotFoundException e)
        {
            System.err.println("Unable to open file at: " + file + "\nSetting Default configuration to 100 height, 100 width, %10 food, 20% lifeforms" );
            try
            {
                System.out.println("Creating default file at:" + "C:\\simulationFiles\\Configuration.txt");
                File tempFile = new File("G:\\pc\\simulationFiles\\Configuration.txt");
                tempFile.createNewFile();
                try
                {


                    FileWriter fileWriter = null;
                    fileWriter = new FileWriter(tempFile);
                    fileWriter.write("600 600 50 100 Wolf 2 MoveFood 20 Hole 20 AnRabbitHole 10 NFood 10 Obs 10 Regrow 20 Poison 30");
                    fileWriter.close();
                    configuration = "600 600 50 100 Wolf 2 MoveFood 20 Hole 20 AnRabbitHole 10 NFood 10 Obs 10 Regrow 20 Poison 30";
                }
                catch (IOException ex)
                {
                    System.err.println("Cannot Create file, Error: "+ ex);
                }
            }
            catch (IOException d)
            {
                System.err.println("Cannot create file, Error" + d);
            }
        }
        catch (IOException e)
        {
            System.err.println("Unable to open file at: " + file  );

        }

    }

    /**
     * Show the entity on the canvas
     * @param image		The image of the entity
     * @param x			The x position
     * @param y			The y position
     */
    public void showEntity(Image image, int x, int y)
    {
        gc.drawImage(image, x, y, 10, 10);
    }

    /**
     * Draw the world on the canvas
     * @param world		The world to draw
     */
    public void drawWorld(AWorld world)
    {
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0,world.getHoriSize()+10, world.getVertiSize()+10);
        world.showEntity(this);
    }

    /**
     * simulation function to be repeated in the animation
     */
    public void simLog()
    {

        if(running == 1)
        {
            getMapInfo(theWorld);
            if(reset != 1 || restart != 1 || pause != 1)
            {
                for(int counter = 0; counter < theWorld.getEntitiesLength();counter++)
                {
                    if( theWorld.getEntitiesLength() != 0)
                    {
                        theWorld.entityAction(counter);
                    }
                }
            }
            if(reset == 1)
            {
                gc.clearRect(0,0,canvas.getHeight()+100, canvas.getWidth());
                drawWorld(theWorld);
                reset = 0;
                timeline.play();
                timeline.stop();


            }
            if(restart == 1)
            {
                gc.clearRect(0,0,canvas.getHeight()+100, canvas.getWidth());
                drawWorld(theWorld);
                restart = 0;

            }

            if(displayMap == 1)
            {

                timeline.setRate(0.1);


            }
            else if(displayMap == 0)
            {
                timeline.setRate(0.5);
            }
            if(pause == 1)
            {
                timeline.stop();
                pause = 0;
            }

        }



        drawWorld(theWorld);

    }

    /**
     * Gets information about the world
     * @param world		The world that is being targeted
     */
    public void getMapInfo(AWorld world)
    {


        int food = 0;
        int obs = 0;
        int wolf = 0;
        for(int i = 0; i < world.entities.size();i++)
        {
            if(world.entities.get(i) instanceof AnFood)
            {
                food = food + 1;
            }
            else if(world.entities.get(i) instanceof AnObstacle)
            {
                obs = obs + 1;
            }
            else if(world.entities.get(i) instanceof AWolf)
            {
                wolf = wolf + 1;
            }
        }

        wolfCount = wolf;
        foodCount = food;
        obsCount = obs;

    }

    /**
     * A borderpane template for the scenes to use
     * @return	BorderPane
     */
    private BorderPane template()
    {
        BorderPane temp = new BorderPane();
        VBox leftPane = leftPane();				// sets button for left pane of borderpane
        temp.setLeft(leftPane);
        temp.setStyle("-fx-background-color: #2F4F4F;");
        return temp;
    }

    /**
     * A VBox template for scenes to use
     * @return	VBox
     */
    private VBox leftPane()
    {

        VBox top = new VBox(10);
        top.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: white;"+
                "-fx-background-color: #2F4F4F;");
        Button button1 = new Button("File");
        Button button2 = new Button("View");
        Button button3 = new Button("Edit");
        Button button4 = new Button("Simulation");
        Button button5 = new Button("Help");

        /* set button to specific scene*/
        button1.setOnAction(e -> {	all.setScene(scene1);
            running = 0;});
        button2.setOnAction(e -> {	all.setScene(scene2);
            running = 0;});
        button3.setOnAction(e -> {	all.setScene(scene3);
            running = 0;});
        button4.setOnAction(e -> {	all.setScene(scene4);
            running = 0;});
        button5.setOnAction(e -> {	all.setScene(scene5);
            running = 0;});
        /*set button max width*/
        button1.setMaxWidth(Double.MAX_VALUE);
        button2.setMaxWidth(Double.MAX_VALUE);
        button3.setMaxWidth(Double.MAX_VALUE);
        button4.setMaxWidth(Double.MAX_VALUE);
        button5.setMaxWidth(Double.MAX_VALUE);
        // add all buttons to vbox

        Text b1Text = new Text( "Mainmenu for the "
                + "\nsimulation");
        Text b2Text = new Text( "Options for viewing"
                + "\ninformation about"
                + "\nthe simulation");
        Text b3Text = new Text( "Options for editing"
                + "\nthe configuration"
                + "\nof the simulation");
        Text b4Text = new Text( "Options for running"
                + "\nthe simulation");
        Text b5Text = new Text( "Options for viewing"
                + "\ninformation about"
                + "\nthe program and author");


        b1Text.setStyle("-fx-font: 13px Oswald;");
        b2Text.setStyle("-fx-font: 13px Oswald;");
        b3Text.setStyle("-fx-font: 13px Oswald;");
        b4Text.setStyle("-fx-font: 13px Oswald;");
        b5Text.setStyle("-fx-font: 13px Oswald;");

        b1Text.setFill(Color.WHITE);
        b2Text.setFill(Color.WHITE);
        b3Text.setFill(Color.WHITE);
        b4Text.setFill(Color.WHITE);
        b5Text.setFill(Color.WHITE);


        top.getChildren().addAll( button1,b1Text, button2, b2Text, button3, b3Text, button4, b4Text, button5, b5Text);

        return top; // returns vbox

    }

    /**
     * Opens the configuration file
     */
    private void openConfig()
    {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(all);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Error Alert");
        alert.setHeaderText(null);
        alert.setContentText("You have either not chosen a file or the configuration file is invalid");

        try
        {
            FileReader fileReader = new FileReader(file);
            @SuppressWarnings("resource")
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String temp = "";
            while((line = bufferedReader.readLine()) != null) {
                temp = line;
            }
            if(isValidConfiguration(temp))
            {
                configuration = temp;
                try
                {   FileWriter fileWriter = null;
                    fileWriter = new FileWriter(defaultFile);
                    fileWriter.write(temp);
                    fileWriter.close();
                }
                catch (IOException ex)
                {
                    System.err.println("Cannot edit file, Error: "+ ex);
                }
                gc.clearRect(0,0,theWorld.getHoriSize()+10, theWorld.getVertiSize()+10);
                fromText(theWorld);
            }
            else
            {
                alert.showAndWait();
            }
        }
        catch (FileNotFoundException e)
        {
            System.err.println("Unable to open file at" + file );

        }
        catch (IOException e)
        {
            System.err.println("Cannot read file at" + file);
        }
        catch(NullPointerException e)
        {
            System.err.println("No file was chosen. Error: " + e);
            alert.showAndWait();
        }
    }

    /**
     * Saves the configuration file
     */
    private void saveFile()
    {
        if(isValidConfiguration(userConfig.getText()) && userConfig.getText() != null)
        {
            configuration = userConfig.getText();
            Alert saveAlert = new Alert(AlertType.CONFIRMATION);
            saveAlert.setTitle("Save Confirmation");
            saveAlert.setHeaderText("Are you sure you want to save?");
            ButtonType buttonYes = new ButtonType("Yes");
            ButtonType buttonNo = new ButtonType("No");
            saveAlert.getButtonTypes().setAll(buttonYes, buttonNo);
            Optional<ButtonType> result = saveAlert.showAndWait();
            if(result.get() == buttonYes)
            {
                try
                {
                    System.out.println("Works here");
                    FileWriter fileWriter = null;
                    fileWriter = new FileWriter(defaultFile);
                    configuration = userConfig.getText();
                    fileWriter.write(userConfig.getText());
                    fileWriter.close();
                }
                catch (IOException ex)
                {
                    System.err.println("Cannot Create file, Error: "+ ex);
                }
            }
        }
        else
        {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Couldnt save as configuration is invalid or missing");
            alert.showAndWait();
        }

    }

    /**
     * Menubar that gets all other menus
     * @return	MenuBar
     */
    private MenuBar allMenus()
    {
        MenuBar temp = new MenuBar();
        temp.getMenus().addAll(setFile(), setView(),  setHelp());
        return temp;
    }

    /**
     * menu for the simulation
     * @return	Menu
     */
    private Menu setSimulation()
    {
        Menu temp = new Menu("Simulation");
        MenuItem iRun= new MenuItem("Run");
        iRun.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {

                running = 1;
                theWorld.makeTempArr();
                timeline.play();
            }
        });

        MenuItem iPau= new MenuItem("Pause");
        iPau.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                pause = 1;

            }
        });

        MenuItem iRes = new MenuItem("Restart");
        iRes.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                restart = 1;
                theWorld.revertArr();
                timeline.play();
            }
        });

        MenuItem iReset= new MenuItem("Reset and stop");
        iReset.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if(running != 0)
                {
                    reset = 1;
                    displayMap = 0;
                    theWorld.revertArr();
                }

            }
        });

        MenuItem iDis= new MenuItem("Display map at each iteration");
        iDis.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if(displayMap == 1) displayMap = 0;
                else displayMap = 1;

            }
        });

        temp.getItems().addAll(iRun, iPau,iRes, iReset, iDis);
        return temp;
    }

    /**
     * menu for the file
     * @return	Menu
     */
    private Menu setFile()
    {
        Menu file = new Menu("File");

        MenuItem iOpen = new MenuItem("Open Configuration file");
        iOpen.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                openConfig();
            }
        });

        MenuItem iSave = new MenuItem("Save");
        iSave.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                saveFile();
            }
        });

        MenuItem iSaveAs = new MenuItem("Save As");
        iSaveAs.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if(isValidConfiguration(userConfig.getText()) && userConfig.getText() != null)
                {
                    String tempNew = userConfig.getText();
                    FileChooser fileChooser = new FileChooser();

                    //Set extension filter
                    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
                    fileChooser.getExtensionFilters().add(extFilter);
                    try
                    {
                        File file = fileChooser.showSaveDialog(all);
                        if(file != null)
                        {
                            try
                            {
                                FileWriter fileWriter = null;

                                fileWriter = new FileWriter(file);
                                fileWriter.write(tempNew);
                                fileWriter.close();
                            }
                            catch (IOException ex)
                            {
                                System.err.println("Something went wrong with the saving as file");
                            }
                        }
                    }
                    catch (NullPointerException e)
                    {
                        System.err.println("File could not be saved Error:" + e);
                    }
                }
                else
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error");
                    alert.setHeaderText("Couldnt save as configuration is invalid or missing");
                    alert.showAndWait();
                }


            }



        });

        MenuItem iExit = new MenuItem("Exit");
        iExit.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                System.exit(0);

            }
        });
        file.getItems().addAll( iOpen, iSave, iSaveAs, iExit);
        return file;
    }

    /**
     * menu for the view
     * @return	Menu
     */
    private Menu setView()
    {
        Menu temp = new Menu("View");
        MenuItem iDisCon= new MenuItem("Display  / Turn off Configuration information");
        iDisCon.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if(displayConfig == 0) displayConfig = 1;
                else displayConfig = 0;
            }
        });

        MenuItem iDisInfLife = new MenuItem("Display / Turn off information about life forms");
        iDisInfLife.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if(displayLifeFood == 0) displayLifeFood = 1;
                else displayLifeFood = 0;
            }
        });

        MenuItem iDisInfMap = new MenuItem("Display  / Turn off information about map ");
        iDisInfMap.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if(displayMapInfo== 0) displayMapInfo = 1;
                else displayMapInfo = 0;
            }
        });


        temp.getItems().addAll(iDisCon, iDisInfLife, iDisInfMap);
        return temp;
    }

    /**
     * menu for the help
     * @return	Menu
     */
    private Menu setHelp()
    {
        Menu temp = new Menu("Help");
        MenuItem iDisApp= new MenuItem("Display information about application");
        iDisApp.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                Alert alert1 = new Alert(AlertType.INFORMATION);
                alert1.setTitle("Information alert");
                alert1.setHeaderText("Information about the application");
                alert1.setContentText("This application is an artificial life simulation, the simulation has different types of entities which are obstacles, food and lifeforms.");
                alert1.showAndWait();
            }
        });
        MenuItem iDisAuth= new MenuItem("Display information about author");
        iDisAuth.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                Alert alert2 = new Alert(AlertType.INFORMATION);
                alert2.setTitle("Information alert");
                alert2.setHeaderText("Information about the author");
                alert2.setContentText("This application was made by Henry So, A student at the University Of Reading. This is his first application with a GUI interface.");
                alert2.showAndWait();
            }
        });

        temp.getItems().addAll(iDisApp, iDisAuth);
        return temp;

    }

    /**
     * Scene for file
     * @return Scene
     */
    private Scene scene1File()
    {
        Scene temp;
        BorderPane paneScene = template();
        VBox centerPane = new VBox();

        Text centerText = new Text("\n\nWelcome to the artificial life simulation\n"
                + "made by Henry So 24010951\n"
                + "\nThis artificial life simulation will simulate a world"
                + "\nwith foods that are split into normal food, poison food,"
                + "\nmoving food like rabbits, obstacles like rocks and holes "
                + "\nand also lifeforms like wolves."
                + "\n\nTo start press a button on the left.\n\n");
        centerText.setStyle("-fx-font: 30px Oswald;");
        centerText.setFill(Color.WHITE);
        StackPane textContainer = new StackPane(centerText);
        textContainer.setAlignment(Pos.CENTER);


        HBox buttonContainer = new HBox(100);
        Text tabSpace = new Text("\t\t\t\t\t\t\t\t\t");
        Button button1 = new Button("New Configuration");
        button1.setOnAction(e -> all.setScene(scene3));
        button1.setPrefSize(200, 100);
        Button button2 = new Button("Open Configuration");
        button2.setOnAction(e -> openConfig());
        button2.setPrefSize(200, 100);
        Button button3 = new Button("Exit");
        button3.setOnAction(e -> System.exit(0));
        button3.setPrefSize(200, 100);
        buttonContainer.getChildren().addAll(tabSpace,button1,button2,button3);
        centerPane.getChildren().addAll( textContainer, buttonContainer);



        MenuBar menuBar = allMenus();
        paneScene.setTop(menuBar);




        paneScene.setCenter(centerPane);

        //add content to centerPane

        temp = new Scene(paneScene, 1600, 800);




        temp.getStylesheets().add(this.getClass().getResource("/main.css").toExternalForm());
        return temp;

    }

    /**
     * Scene for view
     * @return Scene
     */
    private Scene scene2View()
    {
        Scene temp;
        BorderPane paneScene = template();
        MenuBar menuBar = allMenus();
        paneScene.setTop(menuBar);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information Alert");
        alert.setHeaderText(null);
        alert.setContentText("Setting successfully changed");

        VBox centerPane = new VBox();
        Text centerText = new Text("\n\nView settings\n"
                + "\nHere you can toggle the display options of the simulation\n\n");
        centerText.setStyle("-fx-font: 30px Oswald;");
        centerText.setFill(Color.WHITE);
        StackPane textContainer = new StackPane(centerText);
        textContainer.setAlignment(Pos.CENTER);
        HBox buttonContainer = new HBox(100);
        Text tabSpace = new Text("\t\t\t\t\t\t\t\t\t");
        Button button1 = new Button("Toggle Configuration");
        button1.setOnAction(e -> {if(displayConfig == 0) displayConfig = 1;
        else displayConfig = 0;
            alert.showAndWait();});
        button1.setPrefSize(200, 100);
        Button button2 = new Button("Toggle Entity Information");
        button2.setOnAction(e -> {if(displayLifeFood == 0) displayLifeFood = 1;
        else displayLifeFood = 0;
            alert.showAndWait();});
        button2.setPrefSize(200, 100);
        Button button3 = new Button("Toggle Map Information");
        button3.setOnAction(e -> {if(displayMapInfo == 0) displayMapInfo = 1;
        else displayMapInfo = 0;
            alert.showAndWait();});
        button3.setPrefSize(200, 100);
        buttonContainer.getChildren().addAll(tabSpace,button1,button2,button3);
        centerPane.getChildren().addAll( textContainer, buttonContainer);



        paneScene.setCenter(centerPane);

        //add content to centerPane
        temp = new Scene(paneScene, 1600, 800);

        temp.getStylesheets().add(this.getClass().getResource("/main.css").toExternalForm());
        return temp;

    }

    /**
     * Scene for edit
     * @return Scene
     */
    private Scene scene3Edit()
    {
        Scene temp;
        BorderPane paneScene = template();
        MenuBar menuBar = allMenus();
        menuBar.getMenus().addAll();
        paneScene.setTop(menuBar);

        VBox centerPane = new VBox();
        Text centerText = new Text("\n\nConfiguration Page\n"
                + "\nTo edit your configuration enter the"
                + "\nconfiguration string below in the format"
                + "\nworldXsize worldYsize obstacle% food% entity amount"
                + "\nentities are repeatable for example you can have"
                + "\n(Wolf 2 Hole 10) or even two of the same entity"
                + "\n(Wolf 2 Wolf 2) entity is valid if species name"
                + "\nstarts with O,M,H,A,R,W,P,N\n");
        centerText.setStyle("-fx-font: 30px Oswald;");
        centerText.setFill(Color.WHITE);



        StackPane textContainer = new StackPane(centerText);
        textContainer.setAlignment(Pos.CENTER);
        HBox buttonContainer = new HBox(100);
        Text tabSpace = new Text("\t\t\t\t\t\t\t\t\t");
        Button button1 = new Button("Save and Load");
        button1.setOnAction(e -> {saveFile();
            fromText(theWorld);});
        button1.setPrefSize(200, 100);
        Button button2 = new Button("Reset to default");
        button2.setOnAction(e -> {userConfig.setText(configuration);});
        button2.setPrefSize(200, 100);

        buttonContainer.getChildren().addAll(tabSpace,button1,button2);
        centerPane.getChildren().addAll(textContainer,  userConfig, buttonContainer);
        userConfig.setStyle("-fx-border-style: solid inside;"+
                "-fx-border-width: 2;"
                + "-fx-border-insets: 5;"
                + "-fx-border-radius: 5;"
                + "-fx-border-color: white;"
                + "-fx-background-color: #2F4F4F;");

        paneScene.setCenter(centerPane);

        //add content to centerPane
        temp = new Scene(paneScene, 1600, 800);
        temp.getStylesheets().add(this.getClass().getResource("/main.css").toExternalForm());
        return temp;

    }

    /**
     * Scene for simulation
     * @return Scene
     */
    private Scene scene4Simultion()
    {
        Scene temp;
        BorderPane paneScene = template();
        MenuBar menuBar = allMenus();
        menuBar.getMenus().addAll(setSimulation());
        paneScene.setTop(menuBar);


        final Text entityInfo = new Text("");
        final Text textContentEntity = new Text("");
        final Text mapInfo = new Text("");
        final Text textContentMap = new Text("");
        final Text configInfo = new Text("");
        final Text textContentConfig = new Text("");
        entityInfo.setStyle("-fx-font: 13px Oswald;");
        entityInfo.setFill(Color.WHITE);
        textContentEntity.setStyle("-fx-font: 13px Oswald;");
        textContentEntity.setFill(Color.WHITE);
        mapInfo.setStyle("-fx-font: 13px Oswald;");
        mapInfo.setFill(Color.WHITE);
        textContentMap.setStyle("-fx-font: 13px Oswald;");
        textContentMap.setFill(Color.WHITE);
        configInfo.setStyle("-fx-font: 13px Oswald;");
        configInfo.setFill(Color.WHITE);
        textContentConfig.setStyle("-fx-font: 13px Oswald;");
        textContentConfig.setFill(Color.WHITE);





        VBox rightPane = new VBox();
        rightPane.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: white;"+
                "-fx-background-color: #2F4F4F;");
        ScrollPane rightPaneScroll = new ScrollPane();
        rightPaneScroll.setStyle("-fx-background: #2F4F4F;");
        rightPaneScroll.setPrefWidth(350);
        rightPaneScroll.setPrefHeight(1000);
        rightPaneScroll.setContent(entityInfo);
        rightPane.getChildren().addAll(textContentEntity, rightPaneScroll, textContentMap,mapInfo,textContentConfig,configInfo);
        paneScene.setRight(rightPane);



        Pane centerPane = new Pane();

        paneScene.setCenter(centerPane);
        // Put canvas in the center of the window

        centerPane.getChildren().add(canvas);
        // Bind the width/height property to the wrapper Pane
        canvas.widthProperty().bind(centerPane.widthProperty());
        canvas.heightProperty().bind(centerPane.heightProperty());

        gc = canvas.getGraphicsContext2D();


        theWorld.makeTempArr();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setRate(0.5);
        simulation = new KeyFrame(Duration.seconds(0.01),
                new EventHandler<ActionEvent>()
                {
                    public void handle(ActionEvent event)
                    {
                        String foodText = "";
                        String lifeformText = "\t";
                        String nothing = "";
                        if(displayLifeFood == 1)
                        {
                            textContentEntity.setText("Species\t\txPos\t\tyPos\t\tId\t\tEnergy");
                            for(int i = 0; i < theWorld.entities.size(); i++)
                            {

                                if(theWorld.entities.get(i) instanceof AnLifeform )
                                {
                                    lifeformText =  lifeformText + "\n" + ((AWolf)theWorld.entities.get(i)).infoText();
                                }
                                else if(theWorld.entities.get(i) instanceof AnFood)
                                {
                                    foodText = foodText + "\n" + theWorld.entities.get(i).infoText();
                                }
                            }
                            nothing = lifeformText + foodText;
                            entityInfo.setText(nothing);	// this prints out the display all entities
                        }
                        else
                        {
                            entityInfo.setText("");
                            textContentEntity.setText("");
                        }
                        if(displayMapInfo == 1)
                        {
                            String mapTemp =  foodCount +"\t\t"+ obsCount +"\t\t" +wolfCount;
                            textContentMap.setText("Food\tObstacle\tWolf-lifeform");
                            mapInfo.setText(mapTemp);
                        }
                        else
                        {
                            mapInfo.setText("");
                            textContentMap.setText("");

                        }
                        if(displayConfig == 1)
                        {
                            String configTemp = configuration;
                            configInfo.setText(configTemp);
                            textContentConfig.setText("Configuration");
                        }
                        else
                        {
                            configInfo.setText("");
                            textContentConfig.setText("");
                        }

                        simLog();
                    }
                });

        timeline.getKeyFrames().add(simulation);

        temp = new Scene(paneScene, 1600, 800);
        temp.getStylesheets().add(this.getClass().getResource("/main.css").toExternalForm());
        return temp;

    }

    /**
     * Scene for help
     * @return Scene
     */
    private Scene scene5Help()
    {
        Scene temp;
        BorderPane paneScene = template();
        MenuBar menuBar = allMenus();
        paneScene.setTop(menuBar);

        Alert alert1 = new Alert(AlertType.INFORMATION);
        alert1.setTitle("Information alert");
        alert1.setHeaderText("Information about the application");
        alert1.setContentText("This application is an artificial life simulation, the simulation has different types of entities which are obstacles, food and lifeforms.");


        Alert alert2 = new Alert(AlertType.INFORMATION);
        alert2.setTitle("Information alert");
        alert2.setHeaderText("Information about the author");
        alert2.setContentText("This application was made by Henry So, A student at the University Of Reading. This is his first application with a GUI interface.");



        VBox centerPane = new VBox();
        Text centerText = new Text("\n\nHelp Page\n"
                + "\nYou can find information about the application or the author\n"
                + "by clicking on one of the buttons below\n\n");
        centerText.setStyle("-fx-font: 30px Oswald;");
        centerText.setFill(Color.WHITE);



        StackPane textContainer = new StackPane(centerText);
        textContainer.setAlignment(Pos.CENTER);
        HBox buttonContainer = new HBox(100);
        Text tabSpace = new Text("\t\t\t\t\t\t\t\t\t");
        Button button1 = new Button("Display Information \nabout application");
        button1.setOnAction(e -> {if(displayConfig == 0) displayConfig = 1;
        else displayConfig = 0;
            alert1.showAndWait();});
        button1.setPrefSize(200, 100);
        Button button2 = new Button("Display Information \nabout author");
        button2.setOnAction(e -> {if(displayLifeFood == 0) displayLifeFood = 1;
        else displayLifeFood = 0;
            alert2.showAndWait();});
        button2.setPrefSize(200, 100);

        buttonContainer.getChildren().addAll(tabSpace,button1,button2);
        centerPane.getChildren().addAll( textContainer, buttonContainer);




        paneScene.setCenter(centerPane);

        //add content to centerPane

        temp = new Scene(paneScene, 1600, 800);
        temp.getStylesheets().add(this.getClass().getResource("/main.css").toExternalForm());
        return temp;

    }


    @Override
    /**
     * start function that runs when the application is launched
     */
    public void start(Stage primaryStage)
    {
        makeDefaultLocation();
        this.all = primaryStage;
        primaryStage.setTitle("Javafx simulation");

        //finds location of last config and load, if none then create and load default
        defaultFileTest(defaultFile);
        fromText(theWorld);

        //sets the different scenes
        scene1 = this.scene1File();
        scene2 = this.scene2View();
        scene3 = this.scene3Edit();
        scene4 = this.scene4Simultion();
        scene5 = this.scene5Help();

        //shows default scene1
        primaryStage.setScene(scene1);
        primaryStage.setResizable(false);
        primaryStage.show();
    }






}
