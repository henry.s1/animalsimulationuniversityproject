package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the wolf class
 * it is a child of lifeform
 * it can eat all types of food
 * @author Henry So
 * @version 1.0 Final
 */
public class AWolf extends AnLifeform 
{
	/**
	 * Constructor for AWolf
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AWolf(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) {
		super(s, sy, h, v, u, e, w, i);
	}
	
	/**
	 * variables for AWolf to use
	 */
	private boolean poison;
	private int poisonCount = 0;
	
	/**
	 * Changes poison count depending on the value passed
	 * @param value		the value to add to poisoncount
	 */
	public void changePosion(int value)
	{
		poisonCount = poisonCount + value;
	}
	
	/**
	 * Smells if there is food entities around then returns true if there is
	 * @param d				the direction that entity is facing		
	 * @param range			range in which entity can sense
	 * @return	boolean		returns true if there is food else return false
	 */
	public boolean smellFoodCarni(ADirection d, int range)
	{
		AWorld tempWorld = getWorld();
		int tempX = getxPos(), tempY = getyPos();
		boolean isFood = true;
		boolean isMFood = true;
		boolean isObs = true;
		for(int offset = 0; offset <= range; offset = offset + 10)
		{	
			if(d.equals(ADirection.N))
			{	
				isFood = tempWorld.isFood(tempWorld, tempX, tempY - offset);
				isMFood = tempWorld.isMFood(tempWorld, tempX, tempY - offset);
				isObs = tempWorld.isObs(tempWorld, tempX, tempY - offset);
				if(isObs)
				{
					return false;
				}
				else if(isMFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX, tempY - offset));
					return true;
				}
				else if(isFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX, tempY - offset));
					return true;
				}
			}
			else if(d.equals(ADirection.S))
			{
				isFood = tempWorld.isFood(tempWorld, tempX, tempY + offset);
				isMFood = tempWorld.isMFood(tempWorld, tempX, tempY + offset);
				isObs = tempWorld.isObs(tempWorld, tempX, tempY + offset);
				if(isObs)
				{
					return false;
				}
				else if(isMFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX, tempY + offset));
					return true;
				}	
				else if(isFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX, tempY + offset));
					return true;
				}	
			}
			else if(d.equals(ADirection.E))
			{
				isFood = tempWorld.isFood(tempWorld, tempX + offset, tempY);
				isMFood = tempWorld.isMFood(tempWorld, tempX + offset, tempY);
				isObs = tempWorld.isObs(tempWorld, tempX + offset, tempY);
				if(isObs)
				{
					return false;
				}
				else if(isMFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX + offset, tempY));
					return true;
				}	
				else if(isFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX + offset, tempY));
					return true;
				}
			}
			else if(d.equals(ADirection.W))
			{
				isFood = tempWorld.isFood(tempWorld, tempX - offset, tempY);
				isFood = tempWorld.isFood(tempWorld, tempX - offset, tempY);
				isObs = tempWorld.isObs(tempWorld, tempX - offset, tempY);
				if(isObs)
				{
					return false;
				}
				else if(isMFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX - offset, tempY));
					return true;
				}
				else if(isFood)
				{
					setFoodIndex(tempWorld.getEntityAt(tempX - offset, tempY));
					return true;
				}
			}						
		}
		return false;
	}
	
	/**
	 * Gets the direction in which there is food at
	 * @param range			the range to allow sense
	 * @return	ADirection	the direction for entity to move
	 */
	public ADirection getDirectionFoodCarni(int range)
	{
		
		if(smellFoodCarni(ADirection.N, range))
		{
			return ADirection.N;
			
		}
		else if(smellFoodCarni(ADirection.S,range))
		{
			return ADirection.S;
		}
		else if(smellFoodCarni(ADirection.E,range))
		{
			return ADirection.E;
		}else if(smellFoodCarni(ADirection.W,range))
		{
			return ADirection.W;
		}
		else
		{
			return ADirection.None;
		}
		
	}

	/**
	 * 
	 * @return 	returns the boolean poison
	 */
	public boolean isPoison() {
		return poison;
	}

	/**
	 * @return	returns the poisoncount
	 */
	public int getPoisonCount() {
		return poisonCount;
	}

	/**
	 * Sets if wolf is poisoned by temp
	 * @param poison		Amount of energy that is added on
	 */
	public void setPoison(boolean poison) {
		this.poison = poison;
	}
	
	/**
	 * Sets the poisoncount by amount temp
	 * @param poisonCount		Amount of energy that is added on
	 */
	public void setPoisonCount(int poisonCount) {
		this.poisonCount = poisonCount;
	}

}
