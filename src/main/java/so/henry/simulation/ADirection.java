package so.henry.simulation;

import java.util.Random;


/**
 * This is the enumeration that entities
 * will use for their movement
 * @author Henry So
 * @version 1.0 Final
 */
public enum ADirection
{
	N, 
	S, 
	W, 
	E,
	None;
	
	/**
	 * Chooses a random direction excluding none
	 * @return ADirection	Returns the direction that is chosen
	 */
	public static ADirection randomDirection()
	{
		return new ADirection[]{N, S, W, E}[new Random().nextInt(values().length - 1)]; // returns the length of the enumeration array minus one then selects a random value excluding none
	}	
		
}