package so.henry.simulation;

import javafx.scene.image.Image;
/**
 * This is the regrowfood class
 * it is a child of an food
 * this type of food can
 * renew itself
 * @author Henry So
 * @version 1.0 Final
 */
public class AnRegrowFood extends AnFood {

	/**
	 * Constructor for AnRegrowFood
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AnRegrowFood(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) {
		super(s, sy, h, v, u, e, w, i);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Variables for AnRegrowFood to use
	 */
	boolean hasFood = true;
	int respawnTimer = 0;
	
	
	
	public void increaseTime()
	{
		respawnTimer = respawnTimer + 1;
	}
	
	public void resetTimer()
	{
		respawnTimer = 0;
	}

	/**
	 * @return the hasFood
	 */
	public boolean isHasFood() {
		return hasFood;
	}

	/**
	 * @return the respawnTimer
	 */
	public int getRespawnTimer() {
		return respawnTimer;
	}

	/**
	 * @param hasFood the hasFood to set
	 */
	public void setHasFood(boolean hasFood) {
		this.hasFood = hasFood;
	}

	/**
	 * @param respawnTimer the respawnTimer to set
	 */
	public void setRespawnTimer(int respawnTimer) {
		this.respawnTimer = respawnTimer;
	}
	
	
}
