package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the obstacle class
 * it is the parent of all other
 * obstacle classes
 * @author Henry So
 * @version 1.0 Final
 */
public class AnObstacle extends AnEntity 
{

	/**
	 * Constructor for AnObstacle
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */	
	public AnObstacle(String s, char sy, int h, int v, int u, int e, AWorld w,Image i) 
	{
		super(s, sy, h, v, u, e, w,i);
		// TODO Auto-generated constructor stub
	}
	
	

}