package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the AnEntity class it is 
 * the class that all entities will be
 * a child of
 * @author Henry So
 * @version 1.0 Final
 */

public abstract class AnEntity
{
	/*
	 * Variables for AnEntity to use
	 */
	private String species;
	private char symbol;
	private Image image;
	private int xPos, yPos, uniqueId, energy;	
	private  AWorld world;
	private  ADirection direction; 
		
	/**
	 * Constructor for AnEntity
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */	
	AnEntity(String s, char sy, int h, int v, int u, int e, AWorld w, Image i)
	{
		species = s;
		symbol = sy;
		xPos = h;
		yPos = v;
		uniqueId = u;
		energy = e;
		world = w; 
		image = i;
	}
	
	/**
	 * Gets Advanced information of Entity
	 * @return temp 	Returns the species, symbol, Horizontal and Vertical position, unique ID and type
	 */
	public String ToText()
	{
		
		String temp = this.species + " " + this.symbol + " " + this.xPos + " " + this.yPos + " " + this.uniqueId + " " + this.energy;
		return temp;	// return all attributes concatenated
	}
	
	/**
	 * Gets Advanced information of Entity
	 * @return temp 	Returns the species, symbol, Horizontal and Vertical position, unique ID and type
	 */
	public String infoText()
	{
		
		String temp = 	 this.species + 
						"\t\t" + this.xPos + 
						"\t\t" + this.yPos + 
						"\t\t" + this.uniqueId +
						"\t\t" + this.energy + "\n"; 
		return temp;	// return all attributes concatenated
	}
	
	
	
	
	
	/**
	 * Checks if the entity is at a specific x,y position
	 * @param sx	The horizontal position of the entity
	 * @param sy	The vertical position of the entity
	 * @return		Returns true if both entity position and check position is same
	 */
	public boolean isHere (int sx, int sy) 
	{
		return sx == xPos && sy == yPos;			
	}
		
	/**
	 * Used to display the entity in the interface
	 * @param temp	The interface
	 */
	public void displayEntity(MainApp temp)
	{
		temp.showEntity(image, xPos, yPos);
	}
			
	/**
	 * Adds the value of temp to energy
	 * @param temp		Amount of energy that is added on
	 */
	public void changeEnergyPlus(int temp)
	{
		energy = energy + temp;
	}
	
	/**
	 * Minus the value of temp to energy
	 * @param temp		Amount of energy that is added on
	 */
	public void changeEnergyMinus(int temp)
	{
		energy = energy - temp;
	}
	
	/*
	 **********Start of getters and setters*********
	 */
	
	/**
	 * @return 	the species
	 */
	public String getSpecies() {
		return species;
	}
	
	/**
	 * @return 	the symbol
	 */
	public char getSymbol() {
		return symbol;
	}
	
	
	
	/**
	 * @return 	the image
	 */
	public Image getImage() {
		return image;
	}
	
	
	/**
	 * @return 	the xPos
	 */
	public int getxPos() {
		return xPos;
	}
	
	
	/**
	 * @return 	the yPos
	 */
	public int getyPos() {
		return yPos;
	}
	
	/**
	 * @return 	the uniqueId
	 */
	public int getUniqueId() {
		return uniqueId;
	}
	
	/**
	 * @return 	the energy
	 */
	public int getEnergy() {
		return energy;
	}
	
	/**
	 * @return 	the world
	 */
	public AWorld getWorld() {
		return world;
	}
	
	/**
	 * @return 	the direction
	 */
	public ADirection getDirection() {
		return direction;
	}
	
	/**
	 * @param species	 the species to set
	 */
	public void setSpecies(String species) {
		this.species = species;
	}
	
	/**
	 * @param symbol	 the symbol to set
	 */
	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * @param xPos 	the xPos to set
	 */
	public void setxPos(int xPos) {
		this.xPos = xPos;
	}
	
	/**
	 * @param yPos 	the yPos to set
	 */
	public void setyPos(int yPos) {
		this.yPos = yPos;
	}
	
	/**
	 * @param uniqueId	 the uniqueId to set
	 */
	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	/**
	 * @param energy 	the energy to set
	 */
	public void setEnergy(int energy) {
		this.energy = energy;
	}

	/**
	 * @param world 	the world to set
	 */
	public void setWorld(AWorld world) {
		this.world = world;
	}
	
	/**
	 * @param direction 	the direction to set
	 */
	public void setDirection(ADirection direction) {
		this.direction = direction;
	}
	
	/**
	 * @param image		 the image to set
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	
	
	
	
}
