package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the rabbit hole class
 * it is a child of an hole
 * it is used for rabbits to rest
 * or for rabbits to respawn
 * @author Henry So
 * @version 1.0 Final
 */
public class AnRabbitHole extends AnHole {

	/**
	 * Constructor for AnRabbitHole
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AnRabbitHole(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) {
		super(s, sy, h, v, u, e, w, i);
		// TODO Auto-generated constructor stub
	}

}
