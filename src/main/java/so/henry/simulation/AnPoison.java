package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the AnPoison class
 * it is a child on AnFood and 
 * its special property is that 
 * it is a poison food
 * @author Henry So
 * @version 1.0 Final
 */
public class AnPoison extends AnFood {

	/**
	 * Constructor for AnPoison
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AnPoison(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) {
		super(s, sy, h, v, u, e, w, i);
		// TODO Auto-generated constructor stub
	}
	
	
	

}
