package so.henry.simulation;

import java.util.ArrayList;
import java.util.Random;
import javafx.scene.image.Image;

/**
 * This is the AWorld class
 * AWorld is the world that the entities live in
 * all types of entities that is a instance of 
 * an entity can live in the world
 * @author Henry So
 * @version 1.0 Final
 */
public class AWorld 
{
	/**
	 * Variables for the world to use
	 */
	private int horiSize, vertiSize, foodPercent, obsPercent, foodCounter, tempFoodCounter;
	private Random randomGenerator;
	protected ArrayList<AnEntity> entities = new ArrayList<AnEntity>();
	protected ArrayList<AnEntity> tempEntities = new ArrayList<AnEntity>();
	/**
	 * Constructor for AWorld
	 * @param tempHori	The max horizontal position of the world
	 * @param tempVerti	The max vertical position of the world
	 */
	public AWorld(int tempHori, int tempVerti)
	{
		horiSize = tempHori;
		vertiSize = tempVerti;
		foodCounter = 0;
		randomGenerator = new Random();
	}
	
	/**
	 * Creates a temp array to hold entities
	 */
	public void makeTempArr()
	{		
		tempEntities.clear();
		for(int i = 0; i < entities.size(); i++)
		{							
				if(entities.get(i) instanceof AnObstacle)
				{
					if(entities.get(i) instanceof AnHole)
					{
						String temp = entities.get(i).ToText();
						tempEntities.add(newEntity(temp, 'H'));
					}
					else if(entities.get(i) instanceof AnRabbitHole)
					{
						String temp = entities.get(i).ToText();
						tempEntities.add(newEntity(temp, 'A'));
					}
					else
					{
						String temp = entities.get(i).ToText();
						tempEntities.add(newEntity(temp, 'O'));
					}
					
				}
				else if(entities.get(i) instanceof AnMovingFood)
				{
					String temp = entities.get(i).ToText();
					tempEntities.add(newEntity(temp, 'M'));
				}				
				else if(entities.get(i) instanceof AnRegrowFood)
				{
					String temp = entities.get(i).ToText();
					tempEntities.add(newEntity(temp, 'R'));
				}
				else if(entities.get(i) instanceof AWolf)
				{
					String temp = entities.get(i).ToText();
					tempEntities.add(newEntity(temp, 'W'));
				}
				else if(entities.get(i) instanceof AnPoison)
				{
					String temp = entities.get(i).ToText();
					tempEntities.add(newEntity(temp, 'P'));
				}
				else if(entities.get(i) instanceof AnNormalFood)
				{
					String temp = entities.get(i).ToText();
					tempEntities.add(newEntity(temp, 'N'));
				}
				
				
		}
		tempFoodCounter = foodCounter;
	}

	/**
	 * reverts the original array with the use of the temp array
	 */
	public void revertArr()
	{
		foodCounter = tempFoodCounter;
		entities.clear();
		for(int i = 0; i < tempEntities.size(); i++)
		{
				if(tempEntities.get(i) instanceof AnObstacle)
				{
					if(tempEntities.get(i) instanceof AnHole)
					{
						String temp = tempEntities.get(i).ToText();
						entities.add(newEntity(temp, 'H'));
					}
					else if(tempEntities.get(i) instanceof AnRabbitHole)
					{
						String temp = tempEntities.get(i).ToText();
						entities.add(newEntity(temp, 'A'));
					}
					else
					{
						String temp = tempEntities.get(i).ToText();
						entities.add(newEntity(temp, 'O'));
					}
					
				}
				else if(tempEntities.get(i) instanceof AnMovingFood)
				{
					String temp = tempEntities.get(i).ToText();
					entities.add(newEntity(temp, 'M'));
					
				}
				
				else if(tempEntities.get(i) instanceof AnRegrowFood)
				{
					String temp = tempEntities.get(i).ToText();
					entities.add(newEntity(temp, 'R'));
				}
				else if(tempEntities.get(i) instanceof AWolf)
				{
					String temp = tempEntities.get(i).ToText();
					entities.add(newEntity(temp, 'W'));
				}
				else if(tempEntities.get(i) instanceof AnPoison)
				{
					String temp = tempEntities.get(i).ToText();
					entities.add(newEntity(temp, 'P'));
				}
				else if(tempEntities.get(i) instanceof AnNormalFood)
				{
					String temp = tempEntities.get(i).ToText();
					entities.add(newEntity(temp, 'N'));
				}
		}
		

	}
	
	/**
	 * Adds a new entity based on type
	 * @param temp		information about the entity
	 * @param type		type of the entity
	 * @return	AnEntity
	 */
	public AnEntity newEntity(String temp, char type)
	{
		
		String[] TempArr = temp.split(" ");
		for(int counter = 0; counter < TempArr.length; counter++)
		{		
			if(TempArr[counter].charAt(0) == ' ') // if character at position 0 is space then
			{
				TempArr[counter] = TempArr[counter].substring(1, TempArr[counter].length());
			}
		}
		String species = TempArr[0];
		char symbol = TempArr[1].charAt(0);
		int x = -1;
		int y = -1;
		int uniqueId = -1;
		int energy = -1;
		try
		{
			x = Integer.parseInt(TempArr[2]);
			y = Integer.parseInt(TempArr[3]);
			uniqueId = Integer.parseInt(TempArr[4]);
			energy = Integer.parseInt(TempArr[5]);
		}
		catch(NumberFormatException e)
		{
			System.err.println("Error cannot convert:" + e);
		}
		
		Image rabbit = new Image("/rabbit.png");
		Image food = new Image("/food.png");
		Image hole = new Image("/hole.png");
		Image holeRabbit= new Image("/holeRabbit.png");
		Image obs = new Image("/obsrock.png");
		Image regrow = new Image("/regrow.png");
		Image wolf = new Image("/wolf.png");
		Image poison = new Image("/poison.png");
		
		
		
		
		
		if(type == 'N')
		{
			foodCounter++;
			return new AnNormalFood(species, symbol, x ,y, uniqueId, energy, this,food);
			
		}
		else if(type == 'O')
		{
			return new AnObstacle(species, symbol, x ,y, uniqueId, energy, this, obs);
			
		}
		else if(type == 'M')
		{
			foodCounter++;
			return new AnMovingFood(species, symbol, x ,y, uniqueId, 50, this, rabbit);
			
		}
		else if(type == 'H')
		{
			return new AnHole(species, symbol, x ,y, uniqueId, energy, this, hole);
		}
		else if(type == 'A')
		{
			return new AnRabbitHole(species, symbol, x ,y, uniqueId, energy, this, holeRabbit);
		}
		else if(type == 'R')
		{
			foodCounter++;
			return new AnRegrowFood(species, symbol, x ,y, uniqueId, energy, this, regrow);
		}
		else if(type == 'W')
		{
			return new AWolf(species, symbol, x ,y, uniqueId, 100, this, wolf);
		}
		else if(type == 'P')
		{
			foodCounter++;
			return new AnPoison(species, symbol, x ,y, uniqueId, energy, this, poison);
		}
		else
		{
			return null;
		}
	}
			
	/**
	 * Gets the length of the entity array
	 * @return		Returns the length of the entity array
	 */
	public int getEntitiesLength()
	{
		return entities.size();
	}
	
	/**
	 * Sets the world up depending on what parameters there are
	 * @param tempHori		Max horizontal position
	 * @param tempVerti		Max vertical position
	 * @param tempMax		Max number of entities
	 * @param tempFood		The food percentage
	 * @param tempObs		The obs percentage
	 * @return tempWorld	Returns the world that is set
	 */
	public static AWorld setWorld(int tempHori, int tempVerti, int tempMax, int tempFood, int tempObs)
	{
		AWorld tempWorld;		
		
		tempWorld = new AWorld(tempHori, tempVerti);
		tempWorld.setFood(tempFood);
		tempWorld.setObs(tempObs);
		return tempWorld;				
	}
	
	/**
	 * Adds a new entity to the world	
	 * @param temp		The world that the entity is being added to
	 * @param str		The species of the entity
	 * @param c			The symbol of the entity
	 * @param type		The type of the entity
	 */
	public void addNewEntity(AWorld temp, String str, char c, char type)
	{
		if(type == 'O' || type == 'M' || type == 'H' || type == 'A' || type == 'R' || type == 'W' || type == 'N' || type == 'P')
		{
			if(entities.size() < temp.horiSize*temp.vertiSize)
			{
				int tempEne;
				if(type == 'W')
				{
					tempEne = 100;
				}
				else if(type == 'P')
				{
					tempEne = 50;
				}
				else if(type != 'O' || type != 'A')
				{
					tempEne = 5;
				}
				else
				{
					tempEne = 0;
				}
				
				int idTemp = entities.size();
				// find random horiTemp, verti temp
				int tryHori, tryVerti;
				do
				{
						tryHori = (int)(Math.round( this.randomGenerator.nextInt(horiSize) / 10.0) * 10);
						tryVerti = (int)(Math.round( this.randomGenerator.nextInt(vertiSize) / 10.0) * 10);
				}
				while(getEntityAt(tryHori, tryVerti) >= 0);
			
				Image rabbit = new Image("rabbit.png");
				Image food = new Image("/food.png");
				Image hole = new Image("/hole.png");
				Image holeRabbit= new Image("/holeRabbit.png");
				Image obs = new Image("/obsrock.png");
				Image regrow = new Image("/regrow.png");
				Image wolf = new Image("/wolf.png");
				Image poison = new Image("/poison.png");
				
				
				
				if(type == 'N')
				{
					foodCounter++;
					entities.add(new AnNormalFood(str, c, tryHori, tryVerti, idTemp, tempEne, this,food));
					
				}
				else if(type == 'O')
				{
					entities.add(new AnObstacle(str, c, tryHori, tryVerti, idTemp, tempEne, this, obs));
					
				}
				else if(type == 'M')
				{
					foodCounter++;
					entities.add(new AnMovingFood(str, c, tryHori, tryVerti, idTemp, tempEne, this, rabbit));
				}
				else if(type == 'H')
				{
					entities.add(new AnHole(str, c, tryHori, tryVerti, idTemp, tempEne, this, hole));
				}
				else if(type == 'A')
				{
					entities.add(new AnRabbitHole(str, c, 10, 20, idTemp, tempEne, this, holeRabbit));
				}
				else if(type == 'R')
				{
					foodCounter++;
					entities.add(new AnRegrowFood(str, c, tryHori, tryVerti, idTemp, tempEne, this, regrow));
				}
				else if(type == 'W')
				{
					entities.add(new AWolf(str, c, tryHori, tryVerti, idTemp, tempEne, this, wolf));
				}
				else if(type == 'P')
				{
					foodCounter++;
					entities.add(new AnPoison(str, c, tryHori, tryVerti, idTemp, tempEne, this, poison));
				}
			}			
			else
			{
				System.out.println("Max Entities reached");
			}
		}
		else
		{
			System.out.println("No Entity added, you have not entered a valid type" + type);
		}
	}
	
	/**
	 * Gets the entity at a specific position
	 * @param x		The x value to be checked	
	 * @param y		The y value to be checked	
	 * @return ans	Returns the index which the entity is at, if no entity return false
	 */
	public int getEntityAt(int x, int y)
	{
		int index = -1;
		for(int counter = 0; counter < entities.size(); counter++)
		{
			if(entities.get(counter) != null)
			{
				if(entities.get(counter).isHere(x, y))
				{
					index = counter;
					return index;					
				}
				else
				{
					index = -2;
				}
			}
		}
		return index;
	}
	
	public void addRabbitHoleAt(int x, int y)
	{
		
		entities.add(new AnRabbitHole("rabbit hole", 'V', x, y, entities.size(), 10, this, new Image("holeRabbit.png")));
	}
	
	/**
	 * Checks if the entity is food
	 * @param world		World that the entity resides in
	 * @param x			The x position to be checked
	 * @param y			The y position to be checked
	 * @return			Returns true if the entity is food, else returns false
	 */
	public boolean isFood(AWorld world, int x, int y)
	{
		int index = getEntityAt(x, y);
		if(index != -2)
		{
			if(entities.get(index) instanceof AnFood)
			{
				if(entities.get(index) instanceof AnRegrowFood)
				{
					if(((AnRegrowFood)entities.get(index)).hasFood)
					{						
						
						return true;
					}
					else
					{
						
						return false;
					}
				}
				else
				{
					return true;
				}
			}		
		}
		return false;		
	}
	
	/**
	 * Checks if the entity is obstacle
	 * @param world		World that the entity resides in
	 * @param x			The x position to be checked
	 * @param y			The y position to be checked
	 * @return			Returns true if the entity is obstacle, else returns false
	 */
	public boolean isObs(AWorld world, int x, int y)
	{
		int index = getEntityAt(x, y);
		if(index != -2)
		{
			if(entities.get(index) instanceof AnObstacle)
			{
				return true;
			}
		}
		return false;		
	}
	
	/**
	 * Checks if the entity is movingfood
	 * @param world		World that the entity resides in
	 * @param x			The x position to be checked
	 * @param y			The y position to be checked
	 * @return			Returns true if the entity is movingfood, else returns false
	 */
	public boolean isMFood(AWorld world, int x, int y)
	{
		int index = getEntityAt(x, y);
		if(index != -2)
		{
			if(entities.get(index) instanceof AnMovingFood)
			{
				return true;
			}
		}
		return false;		
	}
	
	/**
	 * Checks if the entity is herbfood
	 * @param world		World that the entity resides in
	 * @param x			The x position to be checked
	 * @param y			The y position to be checked
	 * @return			Returns true if the entity is herbfood, else returns false
	 */
	public boolean isHerbFood(AWorld world, int x, int y)
	{
		int index = getEntityAt(x, y);
		if(index != -2)
		{
			
			if(entities.get(index) instanceof AnRegrowFood || entities.get(index) instanceof AnNormalFood)
			{
				if(entities.get(index) instanceof AnRegrowFood)
				{
					if(((AnRegrowFood)entities.get(index)).hasFood)
					{						


						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return true;
				}
			}
		}
		return false;		
	}
	
	/**
	 * Checks if the entity is a hole
	 * @param world		World that the entity resides in
	 * @param x			The x position to be checked
	 * @param y			The y position to be checked
	 * @return			Returns true if the entity is a hole, else returns false
	 */
	public boolean isHole(AWorld world, int x, int y)
	{
		int index = getEntityAt(x, y);
		if(index != -2)
		{
			if(entities.get(index) instanceof AnHole || entities.get(index) instanceof AnRabbitHole)
			{
				
					return true;
				
			}
		}
		return false;		
	}
		
	/**
	 * Checks if the entity can move
	 * @param world		World which entity is in
	 * @param x			The x position to be checked
	 * @param y			The y position to be checked
	 * @return			Returns true if the entity can move, else returns false
	 */
	public boolean canMove(AWorld world, int x, int y)
	{
		// need to change so that it works by bouncing off walls
		int index = world.getEntityAt(x, y);	// your position
		
		// -2 = nothing there
		if(index == -2 || entities.get(index) instanceof AnFood )
		{	
			
				return true;
				
		}
		return false;
		//maybe final should work for everything
	}
	
	/**
	 * Shows the entities
	 * @param temp	The interface which is displaying the entities
	 */
	public void showEntity(MainApp temp)
	{		
		for(int i = 0; i < entities.size(); i++)
		{			
			entities.get(i).displayEntity(temp);			
		}	
	}
	
	/**
	 * Sets the food percentage
	 * @param temp	The number of food
	 */
	public void setFood(int temp)
	{
		foodPercent = (int) (((float)temp/100) * (horiSize*vertiSize));
	}
	
	/**
	 * Sets the Obs percentage
	 * @param temp	The number of Obs
	 */
	public void setObs(int temp)
	{
		obsPercent = (int) (((float)temp/100) * (horiSize*vertiSize));
	}
	
	/**
	 * Initiates the action of the entity
	 * @param index 	The index of the entity affected
	 */
	public void entityAction(int index)
	{
		AnEntity current = entities.get(index);
		if(current instanceof AnFood)
		{
			// do food stuff 
			foodAction(current, index);
		}
		else if(current instanceof AnObstacle)
		{
			if(current instanceof AnRabbitHole)
			{
				obsActionAddRabbit(current, index);
			}
			else
			{
				obsActionAddWolf(current, index);
			}
		}
		else if(current instanceof AnLifeform)
		{		
			lifeformAction(current, index);
		}		
	}
	
	/**
	 * Generates a random number and if the number is 1 then spawn a wolf
	 * @param current			the obstacle that decides to spawn wolf
	 * @param index				the index of the entity
	 */
	public void obsActionAddWolf(AnEntity current, int index)
	{
		// obs has 1 in 100 chance to spawn wolf
		Random rand = new Random();

		int chance = rand.nextInt(10000) + 1;
		if(chance == 1)
		{
			addNewEntity(this, "Wolf", 'W', 'W');
		}
	}
	
	/**
	 * Generates a random number and if the number is 1 then spawn a rabbit
	 * @param current			the hole that decides to spawn rabbit
	 * @param index				the index of the entity
	 */
	public void obsActionAddRabbit(AnEntity current, int index)
	{
		// obs has 1 in 100 chance to spawn wolf
		Random rand = new Random();
		int chance = rand.nextInt(10000) + 1;
		if(chance == 1)
		{
			addNewEntity(this, "MRabbit", 'M', 'M');
		}	
	}
	
	/**
	 * Decides what action a food entity will do
	 * @param current		the food entity
	 * @param index			the index of the entity
	 */
	public void foodAction(AnEntity current, int index)
	{
		if(current instanceof AnMovingFood)
		{
			int energy = current.getEnergy();
				if(energy <= 15)
				{
					((AnMovingFood) current).setRest(true);		
					current.setImage(new Image("rabbitRest.png"));
				}			
				
				if(((AnMovingFood) current).getRest())
				{
					
					if(energy >= 50)
					{
						((AnMovingFood) current).setRest(false);						
					}				
					else
					{
						current.setDirection(((AnMovingFood) current).getHoleDirection(150));
						if(current.getDirection() != ADirection.None)
						{
							((AnMovingFood) current).restHole(current.getDirection(), this);
						}
						current.changeEnergyPlus(1);
					}
				}
				else if(energy > 15)
				{
					current.setImage(new Image("rabbit.png"));
					current.setDirection(((AnMovingFood) current).getDirectionFood(100));	
					if(current.getDirection().equals(ADirection.None))
					{				
						entities.get(index).setDirection(ADirection.randomDirection());			
					}
					else
					{
						if(foodBool(current)) // if true move to that location and delete food
						{
							int foodIndex = getNearEntityIndex(current); // get food index
							current.changeEnergyPlus(entities.get(foodIndex).getEnergy()); // change lifeform energy
							if(entities.get(foodIndex) instanceof AnRegrowFood)
							{							
								if(((AnRegrowFood) entities.get(foodIndex)).hasFood)
								{
									((AnRegrowFood) entities.get(foodIndex)).hasFood = false;
									((AnRegrowFood) entities.get(foodIndex)).setEnergy(0);
									((AnRegrowFood) entities.get(foodIndex)).setImage(new Image("regrowNone.png"));
									((AnRegrowFood) entities.get(foodIndex)).resetTimer();
								}
								
							}
							
							else if(!(entities.get(foodIndex) instanceof AnRegrowFood))
							{
								entities.remove(foodIndex);	// remove food
								this.foodCounter = foodCounter -1;  // remove food
							}						
						}				
					}
					((AnMovingFood) current).moveDirection(current.getDirection(), this);
					current.changeEnergyMinus(1);
				}
			
			
				
	
		}
		
		else if(current instanceof AnRegrowFood)
		{
			if(((AnRegrowFood) current).getRespawnTimer() == 100)
			{
				((AnRegrowFood) current).setEnergy(5);
				((AnRegrowFood) current).resetTimer();
				current.setImage(new Image("regrow.png"));
				((AnRegrowFood) current).hasFood = true;
			}
			else if(((AnRegrowFood) current).getRespawnTimer() < 100)
			{				
				((AnRegrowFood) current).increaseTime();
			}
		}
}
	
	/**
	 * Decides what action a lifeform entity will do
	 * @param current		the lifeform entity
	 * @param index			the index of the entity
	 */
	private void lifeformAction(AnEntity current, int index)
	{
		int energy = current.getEnergy();
		
		if(energy <= 0)
		{
			
				entities.remove(index);
			
		}
		if(energy > 0)
		{
			if(((AWolf) current).isPoison())
			{
				((AWolf) current).changePosion(1);
				if(((AWolf) current).getPoisonCount() == 50)
				{
					((AWolf) current).setPoison(false);
					((AWolf) current).setPoisonCount(0);
				}
			}
			else
			{			
				
					current.setDirection(((AnLifeform) current).getDirectionFood(200));
					if(current.getDirection().equals(ADirection.None))
					{				
						entities.get(index).setDirection( ADirection.randomDirection());
						((AnLifeform) current).moveDirection(current.getDirection(), this);
						current.changeEnergyMinus(1);
						
					}			
					else
					{
						if(foodBool(current)) // if true move to that location and delete food
						{
							int foodIndex = getNearEntityIndex(current); // get food index
							current.changeEnergyPlus(entities.get(foodIndex).getEnergy()); // change lifeform energy
							if(entities.get(foodIndex) instanceof AnRegrowFood)
							{							
								if(((AnRegrowFood) entities.get(foodIndex)).hasFood)
								{
									((AnRegrowFood) entities.get(foodIndex)).hasFood = false;
									((AnRegrowFood) entities.get(foodIndex)).setEnergy(0);
									((AnRegrowFood) entities.get(foodIndex)).setImage(new Image("regrowNone.png"));
									((AnRegrowFood) entities.get(foodIndex)).resetTimer();
								}

							}
							else if(!(entities.get(foodIndex) instanceof AnRegrowFood))
							{
								if(entities.get(foodIndex) instanceof AnPoison)
								{
									((AWolf) current).setPoison(true);
								}
								entities.remove(foodIndex);	// remove food
								this.foodCounter = foodCounter -1;  // remove food
							}
							
							
							
						}
						else if(obsBool(current))
						{
							int obsIndex = getNearEntityIndex(current);
							AnEntity tempObs = entities.get(obsIndex);
							if(tempObs instanceof AnObstacle)
							{
								
							}
						}
						((AnLifeform) current).moveDirection(current.getDirection(), this);
						current.changeEnergyMinus(1);

					
				}
			}
		}	
	}
		
	/**
	 * Checks if the entity is food
	 * @param temp		the entity
	 * @return	boolean
	 */
	private boolean foodBool(AnEntity temp)
	{
		int x = temp.getxPos(), y = temp.getyPos();
		
		if(temp.getDirection().equals(ADirection.N))
		{
			if(isFood(this, x, y - 10))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		if(temp.getDirection().equals(ADirection.S))
		{
			if(isFood(this, x, y + 10))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		if(temp.getDirection().equals(ADirection.E))
		{
			if(isFood(this,x + 10, y))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		if(temp.getDirection().equals(ADirection.W))
		{
			if(isFood(this,x - 10, y))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	
	/**
	 * Checks if the entity is obstacle
	 * @param temp		the entity
	 * @return	boolean
	 */
	public boolean obsBool(AnEntity temp)
	{
		int x = temp.getxPos();
		int y = temp.getyPos();
		if(temp.getDirection().equals(ADirection.N))
		{
			if(isObs(this, x, y - 10))
			{
				return true;
			}
		}
		else if(temp.getDirection().equals(ADirection.S))
		{
			if(isObs(this, x, y + 10))
			{
				return true;
			}
		}
		else if(temp.getDirection().equals(ADirection.E))
		{
			if(isObs(this,x + 10, y))
			{
				return true;
			}
		}
		else if(temp.getDirection().equals(ADirection.W))
		{
			if(isObs(this,x - 10, y))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the entity that is next to the entity temp
	 * @param temp		The that is moving
	 * @return	boolean
	 */
	public int getNearEntityIndex(AnEntity temp)
	{
		int x = temp.getxPos();
		int y = temp.getyPos();
		int index = -2;
		if(temp.getDirection().equals(ADirection.N))
		{	
			index = getEntityAt(x, y-10);
		}
		else if(temp.getDirection().equals(ADirection.S))
		{
			
			index = getEntityAt(x, y+10);
		}
		else if(temp.getDirection().equals(ADirection.E))
		{		
			index = getEntityAt(x + 10, y);
		}
		else if(temp.getDirection().equals(ADirection.W))
		{		
			index = getEntityAt(x - 10, y);
		}
		return index; // if return -2 then no food which is error
	}

	/**
	 * @return the horiSize
	 */
	public int getHoriSize() {
		return horiSize;
	}

	/**
	 * @return the vertiSize
	 */
	public int getVertiSize() {
		return vertiSize;
	}

	/**
	 * @return the foodPercent
	 */
	public int getFoodPercent() {
		return foodPercent;
	}

	/**
	 * @return the obsPercent
	 */
	public int getObsPercent() {
		return obsPercent;
	}

	/**
	 * @return the foodCounter
	 */
	public int getFoodCounter() {
		return foodCounter;
	}

	/**
	 * @return the tempFoodCounter
	 */
	public int getTempFoodCounter() {
		return tempFoodCounter;
	}

	/**
	 * @return the randomGenerator
	 */
	public Random getRandomGenerator() {
		return randomGenerator;
	}

	/**
	 * @return the entities
	 */
	public ArrayList<AnEntity> getEntities() {
		return entities;
	}

	/**
	 * @return the tempEntities
	 */
	public ArrayList<AnEntity> getTempEntities() {
		return tempEntities;
	}

	/**
	 * @param horiSize the horiSize to set
	 */
	public void setHoriSize(int horiSize) {
		this.horiSize = horiSize;
	}

	/**
	 * @param vertiSize the vertiSize to set
	 */
	public void setVertiSize(int vertiSize) {
		this.vertiSize = vertiSize;
	}

	/**
	 * @param foodPercent the foodPercent to set
	 */
	public void setFoodPercent(int foodPercent) {
		this.foodPercent = foodPercent;
	}

	/**
	 * @param obsPercent the obsPercent to set
	 */
	public void setObsPercent(int obsPercent) {
		this.obsPercent = obsPercent;
	}

	/**
	 * @param foodCounter the foodCounter to set
	 */
	public void setFoodCounter(int foodCounter) {
		this.foodCounter = foodCounter;
	}

	/**
	 * @param tempFoodCounter the tempFoodCounter to set
	 */
	public void setTempFoodCounter(int tempFoodCounter) {
		this.tempFoodCounter = tempFoodCounter;
	}

	/**
	 * @param randomGenerator the randomGenerator to set
	 */
	public void setRandomGenerator(Random randomGenerator) {
		this.randomGenerator = randomGenerator;
	}

	/**
	 * @param entities the entities to set
	 */
	public void setEntities(ArrayList<AnEntity> entities) {
		this.entities = entities;
	}

	/**
	 * @param tempEntities the tempEntities to set
	 */
	public void setTempEntities(ArrayList<AnEntity> tempEntities) {
		this.tempEntities = tempEntities;
	}
	
	
	
	
	
}
