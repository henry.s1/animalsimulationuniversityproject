package so.henry.simulation;

import javafx.scene.image.Image;

/**
 * This is the lifeform class
 * this class is the parent of
 * all other lifeform classes life wolf
 * @author Henry So
 * @version 1.0 Final
 */
public class AnLifeform extends AnEntity 
{
	/**
	 * Variables for AnLifeform to use
	 */
	private int foodIndex;
	
	/**
	 * Constructor for AnLifeform
	 * @param s 	Species
	 * @param sy 	Symbol
	 * @param h 	Horizontal Position
	 * @param v 	Vertical Position
	 * @param e 	Energy
	 * @param w 	World 
	 * @param i 	Image
	 * @param u		unique id
	 */
	public AnLifeform(String s, char sy, int h, int v, int u, int e, AWorld w, Image i) 
	{
		super(s, sy, h, v, u, e, w,i);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Smell food function for lifeforms to search for food in a given range
	 * @param d			Direction in which lifeform is facing
	 * @param range		Range that the lifeform can smell from
	 * @return boolean	True if there is food in that direction else return false
	 */
	public boolean smellFood(ADirection d, int range)
	{
		AWorld tempWorld = getWorld();
		int tempX = getxPos(), tempY = getyPos();
		boolean isFood = true;
		boolean isObs = true;
		for(int offset = 0; offset <= range; offset = offset + 10)
		{	
			if(d.equals(ADirection.N))
			{					
				isFood = tempWorld.isFood(tempWorld, tempX, tempY - offset);
				isObs = tempWorld.isObs(tempWorld, tempX, tempY - offset);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX, tempY - offset);
					return true;
				}
			}
			else if(d.equals(ADirection.S))
			{
				isFood = tempWorld.isFood(tempWorld, tempX, tempY + offset);
				isObs = tempWorld.isObs(tempWorld, tempX, tempY + offset);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX, tempY + offset);
					return true;
				}											
			}
			else if(d.equals(ADirection.E))
			{
				isFood = tempWorld.isFood(tempWorld, tempX + offset, tempY);
				isObs = tempWorld.isObs(tempWorld, tempX + offset, tempY);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX + offset, tempY);
					return true;
				}	
			}
			else if(d.equals(ADirection.W))
			{
				isFood = tempWorld.isFood(tempWorld, tempX - offset, tempY);
				isObs = tempWorld.isObs(tempWorld, tempX - offset, tempY);
				if(isObs)
				{
					return false;
				}
				else if(isFood)
				{
					foodIndex = tempWorld.getEntityAt(tempX - offset, tempY);
					return true;
				}
			}						
		}
		return false;
	}
		
	/**
	 * Get the direction a food is in
	 * @param range			The range that the lifeform can sense from
	 * @return	ADirection	The direction that the food is in
	 */
	public ADirection getDirectionFood(int range)
	{
		
		if(smellFood(ADirection.N, range))
		{
			return ADirection.N;
			
		}
		else if(smellFood(ADirection.S,range))
		{
			return ADirection.S;
		}
		else if(smellFood(ADirection.E,range))
		{
			return ADirection.E;
		}else if(smellFood(ADirection.W,range))
		{
			return ADirection.W;
		}
		else
		{
			return ADirection.None;
		}
		
	}
	
	/**
	 * Move lifeform in a given direction
	 * @param d				The direction to move in
	 * @param tempWorld		The world to move in
	 */
	public void moveDirection(ADirection d, AWorld tempWorld)
	{
		int tempX = getxPos(), tempY = getyPos();
		if(d.equals(ADirection.N))
		{
			if(tempWorld.canMove(tempWorld, tempX, tempY - 10))
				{			
					if((tempY - 10) > 0)
					{
						tempY = tempY -10;
					}	
				}
		}
		else if(d.equals(ADirection.S))
		{
			if(tempWorld.canMove(tempWorld, tempX, tempY + 10))
			{			
				if((tempY + 10) < tempWorld.getVertiSize())
				{
					tempY = tempY + 10;
				}
			}		
		}
		else if(d.equals(ADirection.E))
		{
			if(tempWorld.canMove(tempWorld, tempX + 10 , tempY))
			{			
				if((tempX + 10) < tempWorld.getHoriSize())
				{
					tempX = tempX + 10;
				}
			}	
		}
		else if(d.equals(ADirection.W))
		{
			if(tempWorld.canMove(tempWorld, tempX - 10 , tempY))
			{					
				if((tempX - 10) > 0)
				{
					tempX = tempX - 10;
				}
			}	
		}
	}
	
	/**
	 * Get the foood index
	 * @return	foodIndex
	 */
	public int getFoodIndex() {
		return foodIndex;
	}

	/**
	 * set the food index
	 * @param foodIndex		The index of the food
	 */
	public void setFoodIndex(int foodIndex) {
		this.foodIndex = foodIndex;
	}
		
	
	
}
